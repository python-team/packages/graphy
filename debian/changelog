graphy (1.0+dfsg-4) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 05 May 2013 16:00:59 +0200

graphy (1.0+dfsg-3) unstable; urgency=low

  * Python transition:
    - Add support for python2.7.
    - Drop support for python2.5.
  * Bump Standards-Version to 3.9.2. No changes were required.
  * Update copyright file in order to apply correctly Apache 2.0 license.
  * Remove unnecessary Breaks on ${python:Breaks}.

 -- Miguel Landaeta <miguel@miguel.cc>  Tue, 19 Apr 2011 19:03:43 -0430

graphy (1.0+dfsg-2) unstable; urgency=low

  * Bumped Standards-Version to 3.9.1. No changes were needed.
  * Fixed some binary-control-field-duplicates-source lintian warnings.
  * Update watch file.
  * Remove hardcoded /usr/lib/python2.5/ paths. (Closes: #614496).
  * Remove unneeded Provides: ${python:Provides}.
  * Replace dh_pysupport with dh_python2.
  * Fix version for Build-Depends on debhelper.
  * Update minimal version for Build-Depends on python-all to (>= 2.6.6-3~).
  * Enable unit tests.
  * Switched source package format to 3.0 (quilt).
  * Remove deprecated XS-Python-Version and XB-Python-Version fields.
  * Modify copyright file to make it DEP5 compliant and update dates.
  * Add get-orig-source target for convenience.

 -- Miguel Landaeta <miguel@miguel.cc>  Sun, 06 Mar 2011 22:46:53 +0200

graphy (1.0+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #563849).

 -- Miguel Landaeta <miguel@miguel.cc>  Tue, 05 Jan 2010 19:11:14 -0430
